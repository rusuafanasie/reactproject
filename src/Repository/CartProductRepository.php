<?php

namespace App\Repository;

use App\Entity\CartProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method CartProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method CartProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method CartProduct[]    findAll()
 * @method CartProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CartProduct::class);
    }

    // /**
    //  * @return CartProduct[] Returns an array of CartProduct objects
    //  */

    public function findSumOfQuantity($cartId)
    {
        return $this->createQueryBuilder('CP')
            ->select('SUM(CP.quantity)')
            ->where('CP.cart = :id')
            ->setParameter('id', $cartId)
            /*->orderBy('c.id', 'ASC')*/
            /*->setMaxResults(10)*/
            ->getQuery()
            ->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);
    }

    public function findProductByCart(int $cart) {
        $qb = $this->createQueryBuilder('CP');
        $totalPriceByQuantity = $qb->expr()->prod('P.price', 'CP.quantity');

        return $qb
            ->select(
                'CP.id',
                'P.name',
                'CP.quantity',
                "$totalPriceByQuantity as totalPriceByProduct",
                'C.symbol')
            ->join('CP.product', 'P', Join::WITH, 'P.id = CP.product')
            ->join('P.currency', 'C')
            ->where("CP.cart = $cart")
            ->getQuery()
            ->getResult();
    }

    public function findTotalPriceByCart(int $cart) {
        $qb = $this->createQueryBuilder('CP');
        $totalPriceByQuantity = $qb->expr()->prod('P.price', 'CP.quantity');

        return $this->createQueryBuilder('CP')
            ->select("SUM($totalPriceByQuantity)")
            ->join('CP.product', 'P', Join::WITH, 'P.id = CP.product')
            ->where("CP.cart = $cart")
            ->getQuery()
            ->getSingleScalarResult();
    }


    /*
    public function findOneBySomeField($value): ?CartProduct
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
