<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{

    private $registry;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);

        $this->registry = $registry;
    }

     /**
      * @return Product[] Returns an array of Product objects
      */
    public function findAllButNoDescription()
    {

        return $this->createQueryBuilder('p')
            ->select('p.id', 'p.name','p.price','p.imageSource', 'p.stock')
            ->addSelect('pc.name as category')
            ->addSelect('cu.name as currencyName')
            ->addSelect('cu.symbol as currencySymbol')
            ->innerJoin('p.category', 'pc', Join::WITH, 'pc.id = p.category')
            ->innerJoin('p.currency', 'cu', Join::WITH, 'cu.id = p.currency')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
