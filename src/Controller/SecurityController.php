<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    /**
     * @Route("/register")
     */
    public function register() {}

    /**
     * @Route("/login")
     */
    public function login() {}

    /**
     * @Route("/logout")
     */
    public function logout() {}
}