<?php


namespace App\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     * @Template("base.html.twig")
     * @return array
     */
    public function index() {
        $user = $this->getUser();
        return ['user' => $user ? $user->getEmail() : $user];
    }

    /**
     * @Route("/dashboard")
     * @Template("base.html.twig")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dashboard() {
        return $this->redirectToRoute("main",['route' => '/dashboard']);
    }
}