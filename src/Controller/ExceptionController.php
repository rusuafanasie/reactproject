<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\Yaml\Yaml;

class ExceptionController extends AbstractController
{
    public function showAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger) {

        $currentPath = __DIR__;
        $routes = Yaml::parseFile("$currentPath\..\..\assets\yaml\\reactPaths.yaml");
        if (in_array($request->getPathInfo(), $routes))
            return $this->redirectToRoute("main",['route' => $request->getPathInfo()]);
        return $this->json(['404' => 'Page not found']);
    }
}