<?php


namespace App\Controller;


use App\Entity\CartProduct;
use App\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/cardProduct", methods={"GET"})
     */
    public function getProducts() {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository(Product::class)->findAllButNoDescription();

        return $this->json($products);
    }

    /**
     * @Route("/api/user", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function getUserInformation() {
        $user = $this->getUser();
        $cart = $user->getCart();

        return $this->json([
            'email' => $user->getEmail(),
            'cartId' => $cart->getID(),
            'cartStock' => $cart->getStock()
        ]);
    }

    /**
     * @Route("/api/addItem", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addProductInCart(Request $request) {
        $json = $this->verifyJsonRequest($request);

        if ($json['valid']) {
            $data = $json['data'];

            $em = $this->getDoctrine()->getManager();
            $product = $em->find(Product::class, $data['product']);
            if (!$product)
                return $this->json(['error' => 'Product not found'], Response::HTTP_BAD_REQUEST);

            $cart = $this->getUser()->getCart();

            $cartProductRepo = $em->getRepository(CartProduct::class);
            $cartProduct = $cartProductRepo->findOneBy([
                'cart' => $cart->getId(),
                'product' => $product->getId()
            ]);
            $cartProduct = $cartProduct ? $cartProduct : new CartProduct();

            $cartProduct->setProducts($product);
            $cartProduct->setCart($cart);
            $cartProduct->setQuantity($data['quantity']);

            $em->persist($cartProduct);

            $em->flush();

            $stock = $cartProductRepo->findSumOfQuantity($cart->getId());
            $stock = $stock ? $stock : 0;

            $cart->setStock($stock);

            $em->persist($cart);

            $em->flush();

            return $this->json(['cartStock' => $this->getUser()->getCart()->getStock()]);
        } else return $this->json($json, Response::HTTP_BAD_REQUEST);

    }

    /**
     * @Route("/api/cartProducts", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getCartProducts() {

        $cart = $this->getUser()->getCart();

        $em = $this->getDoctrine()->getManager();
        $cartProductsRepo = $em->getRepository(CartProduct::class);

        $cartProducts = $cartProductsRepo->findProductByCart($cart->getId());
        $cartProducts = $cartProducts ? $cartProducts : [];

        if (count($cartProducts))
            $cartProducts['total'] = $cartProductsRepo->findTotalPriceByCart($cart->getId());


        return $this->json($cartProducts);
    }

    /**
     * @Route("/api/removeCartProduct", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function removeCartProduct(Request $request) {
        $json = $this->verifyJsonRequest($request);

        if ($json['valid']) {
            $data = $json['data'];
            $cart = $this->getUser()->getCart();

            $em = $this->getDoctrine()->getManager();

            $cartProductRepo = $em->getRepository(CartProduct::class);

            $cardProduct = $cartProductRepo
                ->find($data['product']);

            $em->remove($cardProduct);
            $em->flush();

            $stock = $em
                ->getRepository(CartProduct::class)
                ->findSumOfQuantity($cart->getId());
            $stock = $stock ? $stock : 0;

            $cart->setStock($stock);

            $em->persist($cart);
            $em->flush();

            $cartProducts = $cartProductRepo->findProductByCart($cart->getId());
            $cartProducts = $cartProducts ? $cartProducts : [];

            return $this->json([
                'cartStock' => $cart->getStock(),
                'cartProducts' => $cartProducts
            ]);
        } else return $this->json($json, Response::HTTP_BAD_REQUEST);
    }

    private function verifyJsonRequest(Request $request) : array {
        $return = ['valid' => false];
        if ($request->getContentType() === 'json') {
            try {
                $jsonD = new JsonEncoder();
                $data = $jsonD->decode($request->getContent(), $jsonD::FORMAT);

                $return['valid'] = true;
                $return['data'] = $data;
                return $return;
            } catch (NotEncodableValueException $notEncodableValueException) {
                $return['error'] = 'Json is not valid';
                return $return;
            }

        }

        $return['error'] = 'Content type not supported';
        return $return;
    }
}