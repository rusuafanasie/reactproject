<?php


namespace App\Security;


use App\Entity\Cart;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GuardAuthenticator extends AbstractGuardAuthenticator
{
    const ERROR_MESSAGES = [
        'general' => 'Credentials where not found',
        'email' => 'Email address not found',
        'password' => 'Password is incorrect',
        'passwordMatch' => 'Passwords do not match',
        'emailExist' => [
            'type' => 'email',
            'message' => 'Email is not available'
        ]
    ];

    private $passwordEncoder;

    private $em;

    private $validator;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $entityManager;
        $this->validator = $validator;
    }

    public function supports(Request $request)
    {
        return ($request->getPathInfo() == '/register' ||
            $request->getPathInfo() == '/login') &&
            $request->isMethod('POST');
    }

    /**
     * Returns a response that directs the user to authenticate.
     *
     * This is called when an anonymous request accesses a resource that
     * requires authentication. The job of this method is to return some
     * response that "helps" the user start into the authentication process.
     *
     * Examples:
     *
     * - For a form login, you might redirect to the login page
     *
     *     return new RedirectResponse('/login');
     *
     * - For an API token authentication system, you return a 401 response
     *
     *     return new Response('Auth header required', 401);
     *
     * @param Request $request The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            'message' => 'Auth required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Get the authentication credentials from the request and return them
     * as any type (e.g. an associate array).
     *
     * Whatever value you return here will be passed to getUser() and checkCredentials()
     *
     * For example, for a form login, you might:
     *
     *      return [
     *          'username' => $request->request->get('_username'),
     *          'password' => $request->request->get('_password'),
     *      ];
     *
     * Or for an API token that's on a header, you might use:
     *
     *      return ['api_key' => $request->headers->get('X-API-TOKEN')];
     *
     * @param Request $request
     * @return mixed Any non-null value
     *
     */
    public function getCredentials(Request $request)
    {
        $jsonD = new JsonEncoder();
        $data = null;


        try {
            if ($request->headers->contains('Content-Type', 'application/json') ||
            $request->headers->contains('Content-Type', 'application/json;charset=UTF-8'))
            $data = $jsonD->decode($request->getContent(), $jsonD::FORMAT);
            else $data = [];
        } catch (NotEncodableValueException $notEncodableValueException) {
            return [];
        }

        $data['requestPath'] = $request->getPathInfo();

        return $data;
    }

    /**
     * Return a UserInterface object based on the credentials.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * You may throw an AuthenticationException if you wish. If you return
     * null, then a UsernameNotFoundException is thrown for you.
     *
     * @param mixed $credentials
     *
     * @param UserProviderInterface $userProvider
     * @return UserInterface|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = null;
        $path = $credentials['requestPath'];
        unset($credentials['requestType']);

        if (empty($credentials)) $this->authenticationIssue('general');

        if ($path == '/login')
            try {
                $user = $userProvider->loadUserByUsername($credentials['email']);
            } catch (UsernameNotFoundException $exception) {
                $this->authenticationIssue('email');
            }
        else if ($path == '/register') {

            $user = $this->em
                ->getRepository(User::class)
                ->findOneByEmail($credentials['email']);

            $user && $this->authenticationIssue('emailExist');

            $user = new User();
            $user
                ->setEmail($credentials['email'])
                ->setPlainPassword($credentials['password'])
                ->setRepeatPassword($credentials['repeatPassword']);
        }

        return $user;
    }

    /**
     * Returns true if the credentials are valid.
     *
     * If any value other than true is returned, authentication will
     * fail. You may also throw an AuthenticationException if you wish
     * to cause authentication to fail.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * @param mixed $credentials
     *
     * @param UserInterface $user
     * @return bool
     *
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if ($credentials['requestPath'] == '/login') {
            if ($this->passwordEncoder->isPasswordValid($user, $credentials['password'])) return true;
            $this->authenticationIssue('password');
        } else if ($credentials['requestPath'] == '/register') {
            $validatorErrors = $this->validator->validate($user);

            if (count($validatorErrors) > 0) {
                $errorMessages = [];
                foreach ($validatorErrors as $v) {
                    $propertyPath = $v->getPropertyPath();
                    $key = $propertyPath == 'plainPassword' ? 'password' : $propertyPath;
                    $errorMessages[$key] = $v->getMessage();
                }

                $this->authenticationIssue($errorMessages);
            }

            $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPlainPassword()));


            $this->em->persist($user);

            $cart = new Cart();
            $cart->setStock(0);
            $cart->setUser($user);

            $this->em->persist($cart);

            $this->em->flush();

            $user->eraseCredentials();
            return true;
        }
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 403 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     *
     * @param Request $request
     * @param AuthenticationException $exception
     * @return Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse($exception->getMessage(), Response::HTTP_FORBIDDEN, [], true);
    }

    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If you return null, the current request will continue, and the user
     * will be authenticated. This makes sense, for example, with an API.
     *
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey The provider (i.e. firewall) key
     *
     * @return JsonResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $data = [
            'message' => 'Authenticated',
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * Does this method support remember me cookies?
     *
     * Remember me cookie will be set if *all* of the following are met:
     *  A) This method returns true
     *  B) The remember_me key under your firewall is configured
     *  C) The "remember me" functionality is activated. This is usually
     *      done by having a _remember_me checkbox in your form, but
     *      can be configured by the "always_remember_me" and "remember_me_parameter"
     *      parameters under the "remember_me" firewall key
     *  D) The onAuthenticationSuccess method returns a Response object
     *
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }

    private function authenticationIssue($messageType) {
        $jsonEncoder = new JsonEncoder();
        $message = null;

        if (is_string($messageType)) {
            $type = self::ERROR_MESSAGES[$messageType];
            $message = is_array($type) ?
                $jsonEncoder
                    ->encode([$type['type'] => $type['message']], $jsonEncoder::FORMAT):
                $jsonEncoder
                    ->encode([$messageType => self::ERROR_MESSAGES[$messageType]], $jsonEncoder::FORMAT);
        }
        else if (is_array($messageType))
            $message = $jsonEncoder->encode($messageType,$jsonEncoder::FORMAT);

        throw new AuthenticationException($message);
    }
}