<?php

namespace App\DataFixtures;

use App\Entity\Cart;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setEmail('user@user.com')
            ->setPlainPassword('password')
            ->setPassword($this->encoder->encodePassword($user, $user->getPlainPassword()));

        $manager->persist($user);

        $cart = new Cart();
        $cart->setStock(0);
        $cart->setUser($user);

        $manager->persist($cart);

        $manager->flush();
    }
}
