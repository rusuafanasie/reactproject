<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    const PRODUCTS = 5;

    public function load(ObjectManager $manager)
    {
        $products = [];
        for ($i = 0; $i < self::PRODUCTS; $i++) {
            $products[$i] = new Product();
        }

        $products[0]
            ->setName('ASUS Transformer Book 10.1" Tablet with Keyboard, T100TAF, 32GB, Win 8.1')
            ->setCurrency($this->getReference(CurrencyFixtures::REFERENCE))
            ->setCategory($this->getReference(ProductCategoryFixtures::REFERENCE))
            ->setDescription('The ASUS Transformer Book is a useful device driven by a robust 1.33 GHz processor and running on Windows platform. The tablet is equipped with 32 GB memory storage. It also features a convenient 10.1-inch display that provides clear visuals. This ASUS tablet supports Wi-Fi connectivity and lets its users wirelessly surf the Web, check e-mails, or stream videos. Its weight is 2 lbs. The product is available in grey')
            ->setPrice(118.81)
            ->setStock(10)
            ->setImageSource('shop/item1.jpg');

        $products[1]
            ->setName('Nintendo Switch 32GB Gray Console (with Blue and Red Joy-Cons)')
            ->setCurrency($this->getReference(CurrencyFixtures::REFERENCE))
            ->setCategory($this->getReference(ProductCategoryFixtures::REFERENCE))
            ->setDescription('The tablet and the joy-cons have been unwrapped. The charger too.everything else is brand new. Everything is undamaged. Comes with Mario Kart Deluxe physical Game in the console.')
            ->setPrice(450.00)
            ->setStock(5)
            ->setImageSource('shop/item2.png');

        $products[2]
            ->setName('Sony PlayStation 4 Slim 1TB Console - Jet Black')
            ->setCurrency($this->getReference(CurrencyFixtures::REFERENCE))
            ->setCategory($this->getReference(ProductCategoryFixtures::REFERENCE))
            ->setDescription('Log hours of gaming fun on the renowned Sony PlayStation 4. Its slim design makes it easy to stack or tuck away into small spaces. The Sony Playstation 4 uses wireless controllers to make gameplay more extensible and has surround sound capability. Purchase disks or use the whopping iTB hard drive storage to purchase games and store for easy access instead.')
            ->setPrice(41.73)
            ->setStock(3)
            ->setImageSource('shop/item3.jpg');

        $products[3]
            ->setName('Nvidia Shield TV 4K HDR 2017 2nd Gen Media Streamer')
            ->setCurrency($this->getReference(CurrencyFixtures::REFERENCE))
            ->setCategory($this->getReference(ProductCategoryFixtures::REFERENCE))
            ->setDescription('Selling my Nvidia Shield TV. 2017 model with 16GB storage. Includes remote and power adapter. Excellent condition. Works perfectly. Looks almost like new')
            ->setPrice(125.00)
            ->setStock(3)
            ->setImageSource('shop/item4.jpg');

        $products[4]
            ->setName('HAIER Smart TV 50" LED IPS 4K Ultra HD - LE50K6600UA - 1 Year Warranty!!')
            ->setCurrency($this->getReference(CurrencyFixtures::REFERENCE))
            ->setCategory($this->getReference(ProductCategoryFixtures::REFERENCE))
            ->setDescription('A brand-new, unused, unopened, undamaged item in its original packaging (where packaging is applicable). Packaging should be the same as what is found in a retail store, unless the item is handmade or was packaged by the manufacturer in non-retail packaging, such as an unprinted box or plastic bag')
            ->setPrice(499.99)
            ->setStock(7)
            ->setImageSource('shop/item5.jpg');

        foreach ($products as $product) {
            $manager->persist($product);
        }

        $manager->flush();
    }
}
