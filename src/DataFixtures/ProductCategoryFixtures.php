<?php

namespace App\DataFixtures;

use App\Entity\ProductCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductCategoryFixtures extends Fixture
{
    const REFERENCE = 'category';

    public function load(ObjectManager $manager)
    {
        $category = new ProductCategory();

        $category->setName('Electronics');

        $manager->persist($category);

        $manager->flush();

        $this->setReference(self::REFERENCE, $category);
    }
}
