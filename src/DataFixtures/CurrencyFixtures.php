<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CurrencyFixtures extends Fixture
{
    const REFERENCE = 'currency';

    public function load(ObjectManager $manager)
    {
        $currency = new Currency();
        $currency2 = new Currency();

        $currency
            ->setName('EU')
            ->setSymbol('€');

        $manager->persist($currency);

        $currency2
            ->setName('US')
            ->setSymbol('$');

        $manager->persist($currency2);

        $manager->flush();

        $this->addReference(self::REFERENCE, $currency);
    }
}
