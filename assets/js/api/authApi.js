import axios from 'axios';

export function loginAPI(data, callback, errorHandler) {
    axios.post('/login',{
        email: data.email,
        password: data.password
    }).then((response) => {
        response.data.message === "Authenticated" && callback();
    }).catch((error) => {
        errorHandler(error.response.data);
    });
}

export function registerAPI(data, callback, errorHandler) {
    axios.post('/register', {
        email: data.email,
        password: data.password,
        repeatPassword: data.repeatPassword
    }).then((response) => {
        callback();
    }).catch((error) => {
        errorHandler(error.response.data, true);
    });
}

export function logoutAPI(callback) {
    axios.post('/logout'
    ).then((response) => {
        callback();
    });
}