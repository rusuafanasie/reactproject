import axios from 'axios';

export function getUser() {
    return axios.get('/api/user').then(({data}) => data);
}

export function getDashboardProducts() {
    return axios.get('/api/cardProduct').then(({data}) => data);
}

export function getProductFromCart() {
    return axios.get('/api/cartProducts').then(({data}) => data);
}

/*
Post Data Object Structure:
item = {
    product: Product id,
    quantity: How many of this type
};
*/
export function postItemInCart(item) {
    return axios.post('/api/addItem',item).then(({data}) => data);
}

/*
Post Data Object Structure:
item = {
    product: Product id from cart_product table,
};
*/
export function postDeleteCartItem(item) {
    return axios.post('/api/removeCartProduct',item).then(({data}) => data);
}