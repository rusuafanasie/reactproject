import React from 'react';
import {
    Navbar,
    NavbarBrand,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavItem,
    NavLink, Collapse
} from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faUser,
    faShoppingCart,
    faTrash
} from '@fortawesome/free-solid-svg-icons';
import { postDeleteCartItem } from "../api/dashboardApi";
import { logoutAPI } from "../api/authApi";
import {Link} from "react-router-dom";
import {REGEX_NON_DIGITS} from '../utils';


export default class NavBar extends React.Component {
    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);

        this.toggleCart = this.toggleCart.bind(this);
        this.deleteCartItem = this.deleteCartItem.bind(this);

        this.state = {
            cartStock: props.cartStock
        };

        this.updatedProducts = null;
    }

    logout() {
        logoutAPI(this.props.logout);
    }

    toggleCart() {
        this.setState({toggleCart: !this.state.toggleCart});
    }

    async deleteCartItem(id, e) {
        e.currentTarget.blur();
        const res = await postDeleteCartItem({product: id});

        if (typeof res.cartStock !== 'undefined'){
            this.setState({cartStock: res.cartStock})
        }
    }

    render() {
        const {props: {match, cartStock, products}} = this;
        const items = [];
        let totalPrice = null;

        if (typeof products === 'object' && products) {
            totalPrice = products.total;

            for (const key in products) {
                !REGEX_NON_DIGITS.test(key) && items.push(products[key]);
            }
        }

        return (
            <Navbar dark color="primary" fixed="top" expand="xs">
                {this.props.logged && <>
                    <NavbarBrand href="/dashboard">My App</NavbarBrand>
                    <Nav className="ml-auto" navbar>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav>
                                <FontAwesomeIcon className="mr-1" icon={faShoppingCart}/> {cartStock}
                            </DropdownToggle>
                            <DropdownMenu right tag="ul">
                                {
                                    Array.isArray(items) && items.length && items.map((prod) => <li className="cart-list-item" key={prod.id}>
                                        <button className="delete-item-btn text-center btn btn-danger" onClick={this.deleteCartItem.bind(null, prod.id)}>
                                            <FontAwesomeIcon icon={faTrash} color="white"/>
                                        </button>
                                        <div style={{top: 0}} className="h-100 w-100 bg-white position-absolute"/>
                                        <DropdownItem className="position-relative">
                                            {prod.name}<div><b>Quantity {prod.quantity}</b><span className="float-right font-italic">{prod.totalPriceByProduct} {prod.symbol}</span></div>
                                        </DropdownItem>
                                        <DropdownItem divider className="position-relative" />
                                    </li>)
                                }
                                {/*ToDo: Total issue because of the currency symbols*/}
                                <h3 className="font-weight-light text-center">Total: {totalPrice} €</h3>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                                <FontAwesomeIcon icon={faUser} />
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem className="disabled">
                                    <Link to={`${match.url}/settings` } className="disabled">Settings</Link>
                                </DropdownItem>
                                <DropdownItem divider />
                                <DropdownItem onClick={this.logout}>
                                    Logout
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </>
                }
            </Navbar>
        );
    }
}