import React, {Component} from 'react';
import {FormFeedback, FormGroup, Input, Label} from "reactstrap";
import {loginAPI, registerAPI} from "../../api/authApi";

export default class AuthForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            invalidMail: false,
            mailMessage: false,
            invalidPassword: false,
            passwordMessage: false,
            repeatPassword: false,
            repeatPasswordMessage: false

        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.errorHandler = this.errorHandler.bind(this);
    }

    handleSubmit(e) {
        const form = e.currentTarget;
        const data = {
            email: form.querySelector('#email').value,
            password: form.querySelector('#password').value
        };

        if (this.props.register) {
            data.repeatPassword = form.querySelector('#repeatPassword').value;
            registerAPI(data, this.props.loginAction, this.errorHandler);
        }
        else loginAPI(data, this.props.loginAction, this.errorHandler);

        e.preventDefault();
    }

    errorHandler(data, register) {
        const states = {
            invalidMail: typeof data.email === 'string',
            mailMessage: data.email,
            invalidPassword: typeof data.password === 'string',
            passwordMessage: data.password
        };

        if (register) {
            states.repeatPassword = typeof data.repeatPassword === 'string';
            states.repeatPasswordMessage = data.repeatPassword;
        }

        this.setState(states);

    }

    render() {
        const state = this.state;

        return <>
            <FormGroup>
                <Label for="email">Email</Label>
                <Input type="email" name="email" id="email" placeholder="Email" invalid={state.invalidMail}/>
                <FormFeedback>{state.mailMessage}</FormFeedback>
            </FormGroup>
            <FormGroup>
                <Label for="password">Password</Label>
                <Input type="password" name="password" id="password" placeholder="Password" invalid={this.state.invalidPassword || state.repeatPassword}/>
                <FormFeedback>{state.passwordMessage}</FormFeedback>
            </FormGroup>
            {this.props.register &&
            <FormGroup>
                <Label for="repeatPassword">Repeat Password</Label>
                <Input invalid={state.repeatPassword} type="password" name="repeatPassword" id="repeatPassword" placeholder="Repeat Password"/>
                <FormFeedback>{state.repeatPasswordMessage}</FormFeedback>
            </FormGroup>}
        </>;
    }
}