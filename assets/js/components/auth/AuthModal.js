import React, {Component} from 'react';
import {Button, Form, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import AuthForm from './AuthForm';

const formType = {
    l: 'Login',
    r: 'Register'
};

export default class AuthModal extends Component {
    constructor() {
        super();

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        this.refs.form.handleSubmit(e);
    }

    render() {
        const {register, toggle} = this.props.details;
        const title = register ? formType.r : formType.l;

        return <Modal isOpen={toggle} toggle={this.props.toggle.bind(null, register)} size="sm">
            <Form onSubmit={this.handleSubmit}>
                <ModalHeader>{title}</ModalHeader>
                <ModalBody>
                    <AuthForm ref="form" register={register} loginAction={this.props.loginAction}/>
                </ModalBody>
                <ModalFooter>
                    <Button color="success" block>{title}</Button>
                </ModalFooter>
            </Form>
        </Modal>;
    }
}