import React, {Component} from 'react';
import {ListGroup, ListGroupItem} from "reactstrap";

export default class Footer extends Component {
    render() {
        return <footer className="d-flex justify-content-around align-items-center">
            <ListGroup flush className="list-group-horizontal">
                <ListGroupItem className="bg-transparent border-0 text-light"><strong>© 2019 Copyright: Afanasie Rusu</strong></ListGroupItem>
            </ListGroup>
        </footer>
    }
}