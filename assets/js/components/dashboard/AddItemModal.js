import React, {Component} from 'react';
import {
    Button,
    Form,
    Input,
    InputGroup,
    InputGroupAddon,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCartPlus} from "@fortawesome/free-solid-svg-icons";

import {DashboardAddItemLoading} from './loading/DashboardLoading';

import {REGEX_NON_DIGITS} from '../../utils';

export default class AddItemModal extends Component {
    constructor(props) {
      super(props);

      this.state = {
          quantity: 1
      };

      this.add = this.add.bind(this);
      this.remove = this.remove.bind(this);
      this.handleQuantity = this.handleQuantity.bind(this);
    };

    add() {
        const {state: {quantity}} = this;
        if (!quantity) return this.setState({quantity: 1});
        if (quantity >= this.props.item.stock ) return null;
        this.setState({quantity: quantity + 1});
    }

    remove() {
        const quantity = parseInt(this.state.quantity);
        if (quantity <= 1 || !quantity) return null;
        this.setState({quantity: this.state.quantity - 1});
    }

    handleQuantity(e) {
        const {props: {item: {stock}}} = this;

        const value = e.target.value;
        const testValue = REGEX_NON_DIGITS.test(value);
        const intValue = parseInt(value);

        if (!value) return this.setState({quantity: value});
        if (testValue) return null;
        if (intValue >= stock) return this.setState({quantity: stock});


        this.setState({quantity: value});
    }

    render() {
        const { props: {modalOpen, item, toggle, actions, loading}} = this;
        let { state: {quantity}} = this;

        if(!modalOpen) this.state.quantity = 1;

        return <Modal isOpen={modalOpen} toggle={toggle}>
                <Form>
                    <ModalHeader className="lead" tag="p">{item.name}</ModalHeader>
                    <ModalBody>
                        <h5 className="text-center">Quantity</h5>
                            <Row>
                                <Col/>
                                <Col xs="8" sm="6">
                                    <InputGroup>
                                        <InputGroupAddon addonType="prepend">
                                            <Button onClick={this.remove} type="button" color="default">-</Button>
                                        </InputGroupAddon>
                                        <Input id="quantity" value={quantity} onChange={this.handleQuantity}/>
                                        <InputGroupAddon addonType="append">
                                            <Button onClick={this.add} type="button" color="default">+</Button>
                                        </InputGroupAddon>
                                    </InputGroup>
                                </Col>
                                <Col/>
                            </Row>
                        <div className="lead text-center">
                            In stock: {item.stock}
                        </div>
                    </ModalBody>
                    <ModalFooter className="p-0 p-lg-3">
                        <Button onClick={actions.addItemRequest.bind(null, quantity)} size="lg" block className="d-lg-none rounded-0 m-0" color="success">
                            {loading ?
                                <DashboardAddItemLoading/> :
                                <FontAwesomeIcon icon={faCartPlus}/>
                            }
                        </Button>
                        <Button onClick={actions.addItemRequest.bind(null, quantity)} block className="d-none d-lg-block" color="success">
                            {loading ?
                                <DashboardAddItemLoading/> : <>
                                    Add <FontAwesomeIcon icon={faCartPlus}/>
                                </>
                            }
                        </Button>
                    </ModalFooter>
                </Form>
        </Modal>
    }
}