import React, {Component} from 'react';
import {
    Button,
    Card,
    CardImg,
    CardBody,
    CardTitle,
    CardSubtitle,
    CardText,
    CardFooter,
    Container, CardDeck
} from "reactstrap";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCartPlus } from '@fortawesome/free-solid-svg-icons'

import {DashboardProdLoading} from './loading/DashboardLoading';

import { getDashboardProducts } from '../../api/dashboardApi';

const imagePathPrefix = '/prod/';

export default class ProductCards extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cardDataLoaded: false,
            something: true,
        };

        this.data = false;
    }
    async componentDidMount() {
        this.data = await getDashboardProducts();
        this.setState({cardDataLoaded: true});
    }

    componentDidUpdate(prevProp, prevState) {
        prevState.cardDataLoaded !== this.state.cardDataLoaded && this.props.footerActive();
    }

    render() {
        const { data, props: {actions}} = this;

        return <Container fluid id="prodCards" className="pt-3 pb-3">
            {!this.state.cardDataLoaded ?
                <DashboardProdLoading/> :
                <CardDeck>
                    {data.map((data) =>
                        <Card key={data.id}>
                            <div className="card-prod-img">
                                <CardImg top src={imagePathPrefix + data.imageSource} alt="Card image cap" />
                            </div>
                            <CardBody>
                                <CardTitle><h6 className="position-relative">{data.name}<div className="card-title-fade"/></h6></CardTitle>
                                <CardSubtitle className="text-info"><b>{data.category}</b></CardSubtitle>
                                <CardText className="text-muted card-prod-price">{data.currencyName} {data.price} {data.currencySymbol}</CardText>
                                <CardText className="text-right">Stock: <span className="prod-stock">{data.stock}</span></CardText>
                            </CardBody>
                            <CardFooter className="p-0 p-lg-3">
                                <Button data-id={data.id} onClick={actions.openAddItemModal} size="lg" className="d-lg-none rounded-0 w-50" color="success">
                                    <FontAwesomeIcon icon={faCartPlus}/>
                                </Button>
                                <Button disabled size="lg" className="d-lg-none rounded-0 w-50" color="primary">Details</Button>
                                <Button data-id={data.id} onClick={actions.openAddItemModal} block className="d-none d-lg-block" color="success">
                                    Add <FontAwesomeIcon icon={faCartPlus}/>
                                </Button>
                                <Button disabled block className="d-none d-lg-block" color="primary" outline>Details</Button>
                            </CardFooter>
                        </Card>
                    )}
                </CardDeck>
            }
        </Container>
    }
}