import React from 'react';
import CircleLoader from 'react-spinners/CircleLoader';
import PulseLoader from 'react-spinners/PulseLoader';

const css = {
    display: 'inline-block'
};

export function DashboardProdLoading() {

    return <div className="text-center">
        <CircleLoader css={css} sizeUnit="px" size="100"/>
    </div>
}

export function DashboardAddItemLoading() {
    return <PulseLoader css={css} color="#fff" sizeUnit="rem" size="0.5"/>
}