import React from "react";
import {
    Jumbotron,
    Row,
    Col,
    Button, Container
} from "reactstrap";
import AuthModal from '../components/auth/AuthModal';
import NavBar from "../components/Nav";

export default class GuestPage extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            register: false,
            toggle: false
        };

        this.modalToggle = this.modalToggle.bind(this);
    }

    modalToggle(type) {
        this.setState({
            register: type,
            toggle: !this.state.toggle});
    }

    render() {
        return <>
            <NavBar logged={false}/>
            <Container fluid>
                <AuthModal details={this.state} toggle={this.modalToggle} loginAction={this.props.login}/>
                <Jumbotron>
                    <h1 className="display-3 text-center">Hello Guest</h1>
                    <hr className="my-2" />
                    <p className="text-center">If you want to use this application. You must have an account to use this app</p>
                    <Row>
                        <Col/>
                        <Col xs="10" sm="6" md="4" lg="3">
                            <Button onClick={this.modalToggle.bind(this, false)} color="success" outline>Login</Button>
                            <Button onClick={this.modalToggle.bind(this, true)} className="float-right" color="primary" outline>Register</Button>
                        </Col>
                        <Col/>
                    </Row>
                </Jumbotron>
            </Container>
        </>
    }
}