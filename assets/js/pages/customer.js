import React from "react";
import {Redirect, Route, Switch, withRouter} from "react-router-dom";

import NavBar from "../components/Nav";
import ProductCarousel from "../components/dashboard/ProductCarousel";
import ProductCard from "../components/dashboard/ProductCards";
import AddItemModal from "../components/dashboard/AddItemModal";
import Footer from "../components/Footer";
import {getUser, postItemInCart, getProductFromCart} from "../api/dashboardApi";

import background from "../../images/background.jpg";
import {Container} from "reactstrap";

class CustomerPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            footer: false,
            cartStock: false,
            loadingCardAddItem: false,
            loadingCartProducts: false,
            addItemModalOpen: false
        };
        this.user = null;
        this.addItem = {
            id: null,
            name: null,
            stock: null
        };
        this.products = null;

        this.footerActive = this.footerActive.bind(this);
        this.openAddItemModal = this.openAddItemModal.bind(this);
        this.toggleAddItemModal = this.toggleAddItemModal.bind(this);
        this.addItemRequest = this.addItemRequest.bind(this);
        this.cartProductRequest = this.cartProductRequest.bind(this);

        this.actions = {
            openAddItemModal: this.openAddItemModal,
            addItemRequest: this.addItemRequest
        };
    }

    footerActive() {
        this.setState({footer: true});
    }

    async componentDidMount() {
        this.user = await getUser();
        this.setState({cartStock: this.user.cartStock});

        this.cartProductRequest();
    }

    async openAddItemModal({target}) {
        const id = parseInt(target.dataset.id);
        if (isNaN(id)) return null;

        const card = target.parentNode.parentNode;
        const stock = parseInt(card.querySelector('.prod-stock').textContent);
        if (isNaN(stock)) return null;

        this.addItem = {
            product: id,
            name: card.querySelector('.card-title > h6').textContent,
            stock
        };


        this.setState({addItemModalOpen: true});
    }

    toggleAddItemModal() {
        this.setState({addItemModalOpen: !this.state.addItemModalOpen});
    }

    async addItemRequest(quantity) {
        if (!quantity) return null;
        this.setState({loadingCardAddItem: true});
        this.addItem.quantity = quantity;

        const res = await postItemInCart(this.addItem);
        this.setState({
            cartStock: res.cartStock,
            loadingCardAddItem: false,
            addItemModalOpen: false,
        });

        this.cartProductRequest();
    }

    async cartProductRequest() {
        this.state.loadingCartProducts = false;

        this.products = await getProductFromCart();

        this.setState({loadingCartProducts: true})
    }

    render() {
        const {props: {match, logOut, isLoggedIn}} = this;
        const {state: {addItemModalOpen, cartStock, loadingCardAddItem, footer}} = this;

        return <> {
            !isLoggedIn ? <Redirect to="/"/> :
                <>
                    <AddItemModal
                        actions={this.actions}
                        modalOpen={addItemModalOpen}
                        item={this.addItem}
                        loading={loadingCardAddItem}
                        toggle={this.toggleAddItemModal}
                    />
                    <Switch>
                        <Route exact path={match.path}>
                            <NavBar
                                products={this.products}
                                cartStock={cartStock}
                                logout={logOut}
                                logged
                                match={match}/>
                            <Container fluid>
                                <img id="background" src={background} alt="No background"/>
                                <ProductCarousel/>
                                <ProductCard
                                    footerActive={this.footerActive}
                                    actions={this.actions}/>
                            </Container>
                        </Route>
                        <Route exact path={`${match.path}/settings`}>
                            <div>Settings</div>
                        </Route>
                    </Switch>
                    {footer && <Footer/>}
                </>
        } </>
    }
}

export default withRouter(CustomerPage);