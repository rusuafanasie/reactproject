export function capitalizeString(string) {
    return typeof string === 'string' ?
    string.charAt(0).toUpperCase() + string.substring(1) : null;
}

export const REGEX_NON_DIGITS = /\D+/;