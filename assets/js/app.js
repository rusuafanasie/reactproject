import React from 'react';
import ReactDom from 'react-dom';
import GuestPage from './pages/guest';
import CustomerPage from './pages/customer';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    withRouter
} from "react-router-dom";


class App extends React.Component {
    constructor(props) {
        super(props);

        const userLogged = !!userEmail;

        this.state = {userLogged};

        this.logOut = this.logOut.bind(this);
        this.logIn = this.logIn.bind(this);
    }

    logOut() {
        this.setState({userLogged: false});
        this.props.history.replace('/');
    }

    logIn() {
        this.setState({userLogged: true});
        this.props.history.replace('/dashboard');
    }

    componentDidMount() {
        const query = this.props.location.search;
        if (query) {
            const splitQuery = query.split('=');
            const route = splitQuery[1];
            this.props.history.replace(route);
        }
        if (this.state.userLogged) this.props.history.replace('/dashboard');
    }

    render() {

        return <Switch>
            <Route exact path="/"><GuestPage login={this.logIn}/></Route>
            <Route path="/dashboard">
                <CustomerPage isLoggedIn={this.state.userLogged} logOut={this.logOut}/>
            </Route>
        </Switch>

    }
}

const MainApp = withRouter(App);

ReactDom.render(<Router><MainApp/></Router>, document.getElementById('root'));

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');
